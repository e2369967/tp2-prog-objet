import csv


def charger_pokémons_csv(pokemon_csv):
    with open(pokemon_csv, 'r') as c:
        fichier_pokemon = csv.reader(c)
        dictionnaire_de_pokemon = {}
        for i in fichier_pokemon:
            pokemon = i[0]
            liste_stats = [int(stat) for stat in i[1:]]
            dictionnaire_de_pokemon[pokemon] = liste_stats
    return dictionnaire_de_pokemon


pkmn = charger_pokémons_csv('pokemon.csv')
for nom, stats in pkmn.items():
    print(f"{nom}: {stats}")

pkmn = charger_pokémons_csv('pokemon.csv')
print(isinstance(pkmn, dict))
print(isinstance(pkmn["Pikachu"], list))
print(isinstance(pkmn["Pikachu"][0], int))

