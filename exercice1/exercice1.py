import json
import csv

with open('data.json', 'r') as j:
    data = json.load(j)

with open('resultat.csv', 'w', newline='') as c:
    csv_en_tete = csv.writer(c)
    csv_en_tete.writerow(['reel', 'imaginaire'])
    for i in data:
        csv_en_tete.writerow(i)